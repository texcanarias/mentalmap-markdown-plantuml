# UML

```plantuml

@startmindmap
* SCRUM
** Roles (Roles)
** Eventos (Events)
*** Sprint
**** Contienen
*** Planificación del Sprint (Sprint planning)
*** Scrum diario (Daily Scrum)
*** Revisión del Sprint (Sprint Review)
*** Retrospectica del Sprint (Sprint Retrospective)
** Artefactos (Artifacts)
** Reglas (Rules)

-- Características
--- Interactivo
--- Incremental

-- Scrum Team
--- Tipos
---- Propietario del producto (Product Owner)
----- Gestiona el Product Backlog
----- Expresar elementos
----- Ordenar elementos para alcanzar
------ Objetivos
------ Misiones
----- Octimizar valor
----- Asegurar entendimiento
----- Única persona
---- Equipo de desarrollo (Development Team)
----- tamaño >=3 && <9
----- autoorganizado
----- multifuncionales
----- no tienen titulos
----- no subequipos
----- habilidades especializadas por individuo
---- Scrum Master
----- Asegurar
------ Objetivo
------ Alcance
------ Dominio
----- Encontrar técnicas gestión
----- Elementos
------ Claros
------ Concisos
----- Entender planificación
----- Como ordenar Product Backlog
----- Entender y planificar agilidad
----- Facilitar y practicar agilidad
--- Características
---- Autorganizado
---- Multifuncional
--- Ventajas
---- Flexibilidad
---- Creatividad
---- Productividad

** Control proceso empírico
*** Transparencia
**** Lenguaje común
**** Definiciones comunes
*** Inspección
**** Frencuente sin interferir
**** Detectar variaciones indeseadas
*** Adaptación
@endmindmap
